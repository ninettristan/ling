# What is it?

ling is a small command-line program for practicing languages and phonology.
ling currently offers two training modes: data mode and phono mode.

## Data mode

In data mode, ling starts by randomly selecting a file under `wordsource`
directory.

Then ling repeatedly:
-   randomly selects a line matching pattern `x|y` from the selected file
-   prints `x` and prompts the user
-   compares user input with y, yielding a result

Data mode can thus be used to train and test one's knowledge of a vocabulary
list formatted like such:

the remainder               | le reste
incidentally                | soit dit en passant
terse                       | laconique
incentive                   | incitatif

## Phono mode

In phono mode, ling starts by randomly selecting a file under `wordsource`
directory.

Then ling repeatedly:
-   randomly selects a word from the selected file
-   gets the word's phonologic transcription
-   prints the word and prompts the user
-   compares user input with the word's phonologic transcription, yielding a
    result

When the user presses ctrl-c, ling prints the user's percentage of correct
answers and exits.

ling currently only supports [british
english](https://en.oxforddictionaries.com/key-to-pronunciation) (Received
Pronunciation) phonology.

ling supports two ways of obtaining a word's phonologic transcription.

If `dictsource = "oxforddictionariesapi"`, ling queries the Oxford Dictionaries
API. To use this, you will need to get an `app_id` and an `app_key` from
[here](https://developer.oxforddictionaries.com/).

If `dictsource = "sdcv"`, ling uses the [sdcv](http://dushistov.github.io/sdcv/)
program. To use this, you will need to have sdcv installed.

Note that the answer is expected to be written in unicode. A not too painful way
to write unicode characters on Linux is to use an .XCompose file.
[Here](https://gitlab.com/snippets/1857076) is an XCompose file containing
sequences for british phonology.

# Usage

Copy configskel.py to config.py. Set options as explained above. That's it, run
ling.

By default ling uses phono mode. To use data mode, use `./ling -t data`.

# Bugs

If you get the warning `RequestsDependencyWarning: urllib3 (1.25.2) or chardet
(3.0.4) doesn't match a supported version!`, then you can downgrade urllib3:

    pip install --upgrade --user urllib3==1.24.3
